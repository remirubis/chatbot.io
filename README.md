# Chatbot.io

Create a chatbot for talking with minimum 3 bots in one canal. Every bot can make minimum 3 actions and all bots have one command in common.
This chat is formated like that:
- On the left we have list of bots
- Ont the right we have conversation history

## Project Installation
### Initiliaze project
```bash
npm i
```

### Network configuration
For usign Socket.io server on all your home network please follow this steps:
- Change line 9 in src/class/Chat.js
```js
let socket = io.connect('0.0.0.0:3000');
```
- Replace global address with your personnal IPV4 address (0.0.0.0 => 192.168.1.XX)

TIPS: For find your personnal IPV4 address use:
```bash
ipconfig //On Windows
```
For mac: go to System Preference > Network > Advanced... > TCP/IP

## Usage

### Launch Socket.io server
```bash
cd server
node server.js
```
### Launch project
```bash
npm run start
```

### Dist creation
```bash
npm run dist
```

### Execute linter
```bash
npm run lint
```

## Bots
In this project, by default 3 bots is generating with differents commands.
#### :partly_sunny: Météo France
Connected to **Openweathermap** API.
| name    | desc                                             | cmds                                      | parameter     | return |
|---------|--------------------------------------------------|-------------------------------------------|---------------|--------|
| Bonjour | Météo France say hello                           | ['hello', 'salut', 'slt', 'coucou', 'cc'] | null          | String |
| Météo   | Weather information for a given location.        | ['meteo', 'weather']                      | lieu - STRING | String |
| Vent    | Know the wind in your city right now.            | ['wind', 'vent']                          | lieu - STRING | String |

#### :heart: Love Animals
Connected to **some-random-api** API.
| name             | desc                       | cmds                                      | parameter | return |
|------------------|----------------------------|-------------------------------------------|-----------|--------|
| Bonjour          | Love Animals say hello     | ['hello', 'salut', 'slt', 'coucou', 'cc'] | null      | String |
| Koala            | See an image of a Koala    | ['koala']                                 | null      | String |
| Chat             | View an image of Cat       | ['cat', 'catty', 'chat']                  | null      | String |
| Loup             | View an image of Wolf      | ['fox', 'loup']                           | null      | String |
| Animal aléatoire | View a random image        | ['random', 'aleatoire', 'magic']          | null      | String |

#### :loop: Randomizer
Connected to **random-data-api** API.
| name       | desc                                                  | cmds                                      | parameter | return |
|------------|-------------------------------------------------------|-------------------------------------------|-----------|--------|
| Bonjour    | Randomizer say hello                                  | ['hello', 'salut', 'slt', 'coucou', 'cc'] | null      | String |
| Bière      | Have the information of a beer randomly               | ['beer', 'biere']                         | null      | String |
| Crypto     | Find a random cryptocurrency to invest in             | ['invest', 'investir', 'crypto']          | null      | String |
| Restaurant | Find a restaurant somewhere in the world              | ['restaurant', 'food', 'eat]              | null      | String |

## :bow: Contributors
:bust_in_silhouette: [Rémi Rubis](https://gitlab.com/remirubis)