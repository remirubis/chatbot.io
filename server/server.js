const server = require('http').createServer();
const io = require('socket.io')(server, {
  cors: {
    origin: '*',
    methods: ['GET', 'POST'],
    credentials: true
  }
});

/**
 * List all connections
 * @type {string: SocketIO.Socket}
 */
const sockets = {};
const onlines = [];
const messages = [];

io.on('connection', (socket) => {
  sockets[socket.id] = socket;

  socket.on('user', (userData) => {
    const payload = {
      id: socket.id,
      ...userData,
      writing: false
    };
    onlines.push(payload);
    socket.emit('socketUpdate', userData);
    socket.emit('joinList', onlines);
    socket.broadcast.emit('join', userData);
  });

  socket.on('getHistory', () => {
    if (messages.length > 0) {
      socket.emit('messages', messages);
    }
  });

  socket.on('message', (msg) => {
    messages.push(msg);
    socket.broadcast.emit('newMsg', msg);
  });

  socket.on('writing', (user, status) => {
    const userWriting = onlines.map((item) => item.id).indexOf(socket.id);
    onlines[userWriting].writing = status;
    socket.broadcast.emit('userWriting', user, status);
    socket.emit('userWriting', user, status);
  });

  socket.on('disconnect', () => {
    delete sockets[socket.id];
    const removeIndex = onlines.map((item) => item.id).indexOf(socket.id);

    if (onlines[removeIndex]) {
      socket.broadcast.emit('leave', onlines[removeIndex].name);
      if (removeIndex >= 0) {
        onlines.splice(removeIndex, 1);
      }
    }
  });
});

server.listen(3000, '0.0.0.0');

module.exports = {
  sockets
};
