import Chat from './class/Chat';
import Bot from './class/Bot';

import './css/style.scss';

const data = [
  {
    name: 'Météo France',
    avatar: 'meteo-france.png',
    actions: [
      {
        title: 'Bonjour',
        desc: 'Salutation de Météo France',
        cmds: ['hello', 'salut', 'slt', 'coucou', 'cc'],
        param: '',
        action() {
          return 'Salut !';
        }
      }, {
        title: 'Météo',
        desc: 'Information météorologique d\'un lieu donné.',
        cmds: ['meteo', 'weather'],
        param: 'Lieu',
        async action(position) {
          return fetch(`https://api.openweathermap.org/data/2.5/weather?q=${position}&appid=5f96d241c1062e0849365d6142f09868&units=metric`)
            .then((res) => res.json())
            .then((dataReturn) => `Voici la météo de ${dataReturn.name} :<br> <strong>Température :</strong> ${dataReturn.main.temp}°<br> <strong>Ressentie :</strong> ${dataReturn.main.feels_like}°<br> <strong>Humidité :</strong> ${dataReturn.main.humidity}%`)
            .catch(() => 'Lieu renseigné inconnu, réessayez en vérifiant l\'orthographe !');
        }
      }, {
        title: 'Vent',
        desc: 'Connaître le vent de votre ville dans l\'immédiat',
        cmds: ['wind', 'vent'],
        param: 'Lieu',
        async action(position) {
          return fetch(`https://api.openweathermap.org/data/2.5/weather?q=${position}&appid=5f96d241c1062e0849365d6142f09868&units=metric`)
            .then((res) => res.json())
            .then((dataReturn) => `Voici la force du vent à ${dataReturn.name} actuellement :<br> <strong>Vitesse :</strong> ${dataReturn.wind.speed} m/s<br> <strong>Direction :</strong> ${dataReturn.wind.deg}<br> <strong>Rafale :</strong> ${dataReturn.wind.gust} m/s`)
            .catch(() => 'Lieu renseigné inconnu, réessayez en vérifiant l\'orthographe !');
        }
      }
    ]
  }, {
    name: 'Love Animals',
    avatar: 'animal.jpeg',
    actions: [
      {
        title: 'Bonjour',
        desc: 'Salutation de Love Animals',
        cmds: ['hello', 'salut', 'slt', 'coucou', 'cc'],
        param: '',
        action() {
          return 'Salut !';
        }
      }, {
        title: 'Koala',
        desc: 'Voir une image de Koala',
        cmds: ['koala'],
        param: '',
        async action() {
          return fetch('https://some-random-api.ml/img/koala')
            .then((res) => res.json())
            .then((dataReturn) => `<img src="${dataReturn.link}" alt="Chargement" style="max-width: 200px; max-height: 200px">`)
            .catch(() => 'Aucune image n\'a pu être trouvée sur le serveur...');
        }
      }, {
        title: 'Chat',
        desc: 'Voir une image de Chat',
        cmds: ['cat', 'catty', 'chat'],
        param: '',
        async action() {
          return fetch('https://some-random-api.ml/img/cat')
            .then((res) => res.json())
            .then((dataReturn) => `<img src="${dataReturn.link}" alt="Chargement" style="max-width: 200px; max-height: 200px">`)
            .catch(() => 'Aucune image n\'a pu être trouvée sur le serveur...');
        }
      }, {
        title: 'Loup',
        desc: 'Voir une image de Loup',
        cmds: ['fox', 'loup'],
        param: '',
        async action() {
          return fetch('https://some-random-api.ml/img/fox')
            .then((res) => res.json())
            .then((dataReturn) => `<img src="${dataReturn.link}" alt="Chargement" style="max-width: 200px; max-height: 200px">`)
            .catch(() => 'Aucune image n\'a pu être trouvée sur le serveur...');
        }
      }, {
        title: 'Animal aléatoire',
        desc: 'Voir une image aléatoire',
        cmds: ['random', 'aleatoire', 'magic'],
        param: '',
        async action() {
          const links = ['https://some-random-api.ml/img/dog', 'https://some-random-api.ml/img/cat', 'https://some-random-api.ml/img/panda', 'https://some-random-api.ml/img/red_panda', 'https://some-random-api.ml/img/birb', 'https://some-random-api.ml/img/fox', 'https://some-random-api.ml/img/koala'];
          const url = links[Math.floor(Math.random() * links.length)];
          return fetch(url)
            .then((res) => res.json())
            .then((dataReturn) => `<img src="${dataReturn.link}" alt="Chargement" style="max-width: 200px; max-height: 200px">`)
            .catch(() => 'Aucune image n\'a pu être trouvée sur le serveur...');
        }
      }
    ]
  }, {
    name: 'Randomizer',
    avatar: 'meme.jpeg',
    actions: [
      {
        title: 'Bonjour',
        desc: 'Salutation de Randomizer',
        cmds: ['hello', 'salut', 'slt', 'coucou', 'cc'],
        param: '',
        action() {
          return 'Salut !';
        }
      }, {
        title: 'Bière',
        desc: 'Avoir les informations d\'une bière aléatoirement',
        cmds: ['beer', 'biere'],
        param: '',
        async action() {
          return fetch('https://random-data-api.com/api/beer/random_beer')
            .then((res) => res.json())
            .then((dataReturn) => `Et voici votre bière<br> <strong>Marque :</strong> ${dataReturn.brand}<br><strong>Nom :</strong> ${dataReturn.name}<br><strong>Style :</strong> ${dataReturn.style}<br><strong>° d'alcool :</strong> ${dataReturn.blg}`)
            .catch(() => 'Oups je n\'ai pas de bière à te donner dans l\'immédiat...');
        }
      }, {
        title: 'Crypto',
        desc: 'Trouver une cryptomonnaie aléatoirement pour investir',
        cmds: ['invest', 'investir', 'crypto'],
        param: '',
        async action() {
          return fetch('https://random-data-api.com/api/crypto_coin/random_crypto_coin')
            .then((res) => res.json())
            .then((dataReturn) => `Voici la crypto qu'il vous faut !<br><strong>Nom :</strong> ${dataReturn.coin_name}<br><strong>Acronyme :</strong> ${dataReturn.acronym}<br>`)
            .catch(() => 'Oups, les crypto s\'ecroulent n\'achetez pas !');
        }
      }, {
        title: 'Restaurant',
        desc: 'Trouver un restaurant quelque part dans le monde',
        cmds: ['restaurant', 'food', 'eat'],
        param: '',
        async action() {
          return fetch('https://random-data-api.com/api/restaurant/random_restaurant')
            .then((res) => res.json())
            .then((dataReturn) => {
              const lines = [
                'Voici un restaurant sympa a tester :',
                `<strong>Name :</strong> ${dataReturn.name}`,
                `<strong>Adresse :</strong> ${dataReturn.address}`,
                '<strong>Horaires :</strong>'
              ];
              const days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
              Object.entries(dataReturn.hours).forEach((day, key) => {
                const info = day[1].is_closed ? 'FERMÉ' : `De ${day[1].opens_at} à ${day[1].closes_at}`;
                lines.push(`<strong>    ${days[key]} :</strong> ${info}`);
              });
              let msg = '';
              lines.forEach((line, key) => {
                msg += `${line} ${key !== lines.length - 1 ? '<br>' : ''}`;
              });
              return msg;
            })
            .catch(() => 'Oups, les crypto s\'ecroulent n\'achetez pas !');
        }
      }
    ]
  }
];

const bots = data.map((bot) => new Bot(bot));
export default new Chat(bots);
