import Message from './Message';
import Action from './Action';

export default class Bot {
  constructor(entity) {
    const {
      name,
      avatar,
      actions
    } = entity;

    this.name = name;
    this.avatar = avatar;
    this.actions = actions;
  }

  /*
   * Execute class action with current bot action, this methods get
   * command and parameter who entered by user. This methods execute help command
   * for all bots too
   *
   * @param {Chat} chat Chat instance for send messages in front in Action class and help command
   * @param {String} userMessage Get all string chain who entered by user
   *
   */
  execActions(chat, userMessage, executorSocket) {
    const messageTable = userMessage.split(' ');
    const cmd = messageTable[0].toLowerCase();
    const param = messageTable[1];
    const helpCmds = ['?', 'help', 'aide'];

    if (helpCmds.includes(cmd)) {
      const allActions = this.actions;
      const msg = new Message('<strong>Nom [Commandes] (Paramètre) : Description</strong>', this);
      allActions.forEach((action) => {
        const { title, desc, cmds } = action;
        let commands = ' [';
        cmds.forEach((command, key) => { commands += command + (key !== action.cmds.length - 1 ? ',' : ''); });
        commands += `] (${action.param}) : `;
        const line = title + commands + desc;
        msg.msg = (msg.msg === '' ? '' : `${msg.msg}<br>`) + line;
      });
      document.querySelector('#messages').innerHTML += chat.receiveMessage(msg);
      document.querySelector('#messages li:last-child').scrollIntoView();
    } else {
      this.actions.map((action) => new Action(cmd, param, chat, this, action, executorSocket));
    }
  }
}
