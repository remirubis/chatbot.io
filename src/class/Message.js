export default class Message {
  constructor(msg, author, createdAt = Date.now(), updatedAt = Date.now()) {
    this.author = author;
    this.msg = msg;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}
