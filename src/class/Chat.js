import User from './User';
import Bot from './Bot';
import Message from './Message';

const io = require('socket.io-client');

// PLEASE CHANGE ME IF YOU WANT ACCESS SERVER FROM LAN NETWORK
// eslint-disable-next-line prefer-const
let socket = io.connect('0.0.0.0:3000', {
  rememberUpgrade: true,
  transports: ['websocket'],
  secure: true,
  rejectUnauthorized: false
});

let chatInstance = {};

export default class Chat {
  constructor(contacts) {
    this.contacts = contacts;
    this.app = document.querySelector('#app');
    this.beforeConnect();
    chatInstance = this;
  }

  /*
   * Return HTML of header parts
   *
   * @return {String} Return HTML code who display in front
   *
   */
  renderHeader() {
    return ` 
      <header>
        <h1>Chatbot.io</h1>
      </header>
    `;
  }

  /*
   * Return correct avatar if bot have or have not a avatar configured,
   * if avatar is empty we generate it with bots name initials.
   *
   * @param {Bot} bot Bots data for display his picture
   *
   * @return {String} Return HTML code who display in front
   *
   */
  avatar(user) {
    if (user.avatar === '') {
      let letters = user.name.split(' ').map((i) => i.charAt(0));
      letters = letters.toString().toUpperCase();
      const render = letters.split(',').join('');
      return `
        <div class="avatar">
          <p class="avatar-txt">${render.substring(0, 2)}</p>
        </div>`;
    }
    return `
      <figure class="avatar">
        <img class="avatar-picture" src="./src/img/${user.avatar}" alt="Avatar de ${user.name}">
      </figure>`;
  }

  /*
   * This methods is used for transform timestamp to a readable date and time
   *
   * @param {Integer} timestamp Timestamp you want to transform to formated date
   *
   * @return {String} Return formated date for display it in front
   *
   */
  transformDate(timestamp) {
    const date = new Date(timestamp).toLocaleString();
    return date;
  }

  /*
   * Return HTML of bot message, used when bot response to a command
   *
   * @param {Message} msg Instance of Message class who added to messages list
   *
   * @return {String} Return HTML code who display in front
   *
   */
  receiveMessage(msg) {
    return `
      <li class="in">
        <div class="chat-img">
          ${this.avatar(msg.author)}
        </div>
        <div class="chat-body">
          <div class="chat-message">
            <h5 class="name">${msg.author.name}</h5>
            <p>${msg.msg}</p>
          </div>
          <p>Envoyé le ${this.transformDate(msg.createdAt)}</p>
        </div>
      </li>
    `;
  }

  /*
   * Return HTML of user message when he used form
   *
   * @param {Message} msg Message generated in writingMessage() methods
   *
   * @return {String} Return HTML code who display in front
   *
   */
  sendMessage(msg) {
    return `
      <li class="out">
        <div class="chat-body">
          <div class="chat-message">
            <h5>${msg.author.name}</h5>
            <p>${msg.msg}</p>
          </div>
          <p>Envoyé le ${this.transformDate(msg.createdAt)}</p>
        </div>
      </li>
    `;
  }

  /*
   * Add previous history on user connection
   *
   * @param {Message[]} msg Array of instance of Message Class would you want to display in chat
   *
   * @return {String} Return HTML code who display in front
   *
   */
  addHistory(msg = []) {
    document.querySelector('#messages').innerHTML += msg.map(
      (message) => (
        message.author.name.toLowerCase() === socket.user.name.toLowerCase()
          ? this.sendMessage(message)
          : this.receiveMessage(message)
      ).split(',', 1)
    );
  }

  /*
   * Render messages area for using chat form and display messages
   *
   * @param {Message[]} msg Array of instance of Message Class would you want to display in chat
   *
   * @return {String} Return HTML code who display in front
   *
   */
  renderMessages(msg = []) {
    return `
      <div class="chat">
        <div class="title">
          <h2>Channel global</h2>
          <p id="connected"><span>0</span> utilisateur connecté</p>
        </div>
        <ul class="chat-list" id="messages">
          ${msg.map((message) => (message.author.name.toLowerCase() === socket.user.name.toLowerCase() ? this.sendMessage(message) : this.receiveMessage(message)).split(','))}
        </ul>
        <div class="chat-bottom">
          <ul class="user-list" id="user-list"></ul>
          <form id="msg-form">
            <input type="text" id="msg" placeholder="Ecrire un message">
            <button type="submit">Envoyer</button>
          </form>
        </div>
      </div>
    `;
  }

  /*
   * Create a contact div, used in renderContacts() methods
   *
   * @param {Bot} contact Bot instance for getting data and display it in contacts area
   *
   * @return {String} Return HTML code who display in front
   *
   */
  addContact(contact) {
    return `
      <li>
        <div class="avatar">
          <p class="avatar-txt">${this.avatar(contact)}</p>
        </div>
        <p>${contact.name}</p>
      </li>
    `;
  }

  /*
   * Create contacts area with all Bots who configured in data
   *
   * @param {Bot[]} contacts Array of Bot instance for use is in addContact() methods
   *
   * @return {String} Return HTML code who display in front
   *
   */
  renderContacts(contacts = []) {
    return `
      <div class="list">
        <div class="title">
          <h2>Bot en ligne</h2>
          <p>${contacts.length}</p>
        </div>
        <ul class="list-bots">
          ${contacts.map((contact) => this.addContact(contact)).join('')}
        </ul>
      </div>
    `;
  }

  /*
   * Methods who used when user enter data in form, getting user informations in socket data
   * and used it for create user to link user with message who created after if message is
   * not empty. Adding a scrolling feature after message sended
   */
  writingMessage() {
    const form = document.getElementById('msg-form');
    const input = document.getElementById('msg');
    input.addEventListener('input', () => {
      socket.emit('writing', socket.user.name, input.value !== '');
    });
    form.addEventListener('submit', (e) => {
      e.preventDefault();

      const userData = socket.user;

      if (userData === null) {
        document.getElementById('msg').value = '';
        this.renderModal();
        return;
      }

      const user = new User(
        userData.name,
        userData.avatar,
        userData.createdAt,
        userData.updatedAt
      );

      const msg = document.getElementById('msg').value;
      if (msg !== '') {
        const newMessage = new Message(msg, user);
        const messageTable = msg.split(' ');
        const cmd = messageTable[0].toLowerCase();
        const helpCmds = ['?', 'help', 'aide'];

        if (!helpCmds.includes(cmd)) {
          socket.emit('message', newMessage);
        }
        document.getElementById('messages').innerHTML += this.sendMessage(newMessage);
        document.querySelector('#messages li:last-child').scrollIntoView();

        this.contacts.map((contact) => contact.execActions(this, msg, socket));

        document.getElementById('msg').value = '';
        socket.emit('writing', socket.user.name, input.value !== '');
      }
    });
  }

  /*
   * Methods who used when user come on website, waiting user
   * enter his username in form and save it socket data.
   */
  usernameForm() {
    const form = document.getElementById('name-form');
    form.addEventListener('submit', (e) => {
      e.preventDefault();
      const username = document.getElementById('name').value;
      if (username !== '') {
        const modal = document.querySelector('.modal');
        modal.classList.add('hidden');

        const user = new User(username);
        socket.emit('user', user);

        this.app.innerHTML += `
          <div class="alert" role="alert">
            Bienvenue ${user.name} !
          </div>`;
        setTimeout(() => {
          const alert = document.querySelector('.alert');
          alert.remove();
        }, 5000);
      }
    });
  }

  /*
   * Running app with all components like header, contacts and chat.
   */
  run() {
    this.app.innerHTML += this.renderHeader();
    this.app.innerHTML += `
      <main>
        ${this.renderContacts(this.contacts)}
        ${this.renderMessages()}
      </main>
    `;
    this.writingMessage();
  }

  beforeConnect() {
    this.app.innerHTML += `
      <div class="modal">
        <h2>Bienvenue, comment doit-on t'appeler ?</h2>
        <div class="modal-body">
          <form id="name-form">
            <div class="input">
              <label for="name">Renseigne ton nom :</label>
              <input type="text" id="name">
            </div>
            <button type="submit">C'est partit !</button>
          </form>
        </div>
      </div>
    `;
    this.usernameForm();
  }
}

/*
 * Create correct instance of Message class with correct author like Bot or user
 *
 * @param {object} data JSON object from socket.io server
 *
 * @return {Message} Return message instance
 *
 */

function createMsgInstance(data) {
  let msgOwner;
  if (data.author.actions !== null) {
    msgOwner = new User(
      data.author.name,
      data.author.avatar,
      data.author.createdAt,
      data.author.updatedAt
    );
  } else {
    msgOwner = new Bot(data.author.name, data.author.avatar, data.author.actions);
  }
  return new Message(data.msg, msgOwner, data.createdAt, data.updatedAt);
}

/*
 * When user join, update his socket and push chat history
 *
 * @param {object} userData User informations from socket.io server
 *
 */
socket.on('socketUpdate', (userData) => {
  socket.user = userData;
  chatInstance.run();
  socket.emit('getHistory');
});

/*
 * Get new message from server and add it with correct style to user
 *
 * @param {object} msg Message who sent from socket.io server
 *
 */
socket.on('newMsg', (msg) => {
  if (socket.user) {
    const newMessage = createMsgInstance(msg);
    document.querySelector('#messages').innerHTML += newMessage.author.name === socket.user.name
      ? chatInstance.sendMessage(newMessage)
      : chatInstance.receiveMessage(newMessage);
    document.querySelector('#messages li:last-child').scrollIntoView();
  }
});

/*
 * Generate with aray all message history for new user
 *
 * @param {Array} messages List of all messages from socket.io server
 *
 */
socket.on('messages', (messages) => {
  const msgFormat = messages.map((single) => createMsgInstance(single));
  chatInstance.addHistory(msgFormat);
  document.querySelector('#messages li:last-child').scrollIntoView();
});

/*
 * Increment connected count and add user to user list behind message input
 * for all connected users
 *
 * @param {object} userData User informations from socket.io server
 *
 */
socket.on('join', (userData) => {
  if (document.querySelector('#connected span')) {
    const onlines = parseInt(document.querySelector('#connected span').textContent, 10) + 1;
    document.querySelector('#connected').innerHTML = `<span>${onlines}</span> utilisateur${onlines > 1 ? 's' : ''}  connecté${onlines > 1 ? 's' : ''}`;
    if (document.querySelector('#user-list')) {
      document.querySelector('#user-list').innerHTML += `
        <li>${userData.name}</li>
      `;
    }
  }
});

/*
 * Increment connected count and add user to user list before message input
 * when user join for new user
 *
 * @param {object} userData User informations from socket.io server
 *
 */
socket.on('joinList', (onlines) => {
  document.querySelector('#connected').innerHTML = `<span>${onlines.length}</span> utilisateur${onlines.length > 1 ? 's' : ''}  connecté${onlines.length > 1 ? 's' : ''}`;
  onlines.forEach((online) => {
    document.querySelector('#user-list').innerHTML += `
      <li ${online.writing ? 'class="writing"' : ''}>${online.name}</li>
    `;
  });
});

/*
 * If someone typing display it for all users
 *
 * @param {String} username Username of user who typing
 * @param {Boolean} status Status of action if is doing or ended
 *
 */
socket.on('userWriting', (username, status) => {
  const userList = document.querySelectorAll('#user-list li');
  userList.forEach((el) => {
    if (el.textContent === username) {
      if (status) {
        el.classList.add('writing');
        return;
      }
      el.classList.remove('writing');
    }
  });
});

/*
 * Remove user of connected list for all users
 *
 * @param {String} username Name of user who leave the chat
 *
 */
socket.on('leave', (username) => {
  const onlines = parseInt(document.querySelector('#connected span').textContent, 10) - 1;
  document.querySelector('#connected').innerHTML = `<span>${onlines}</span> utilisateur${onlines > 1 ? 's' : ''}  connecté${onlines > 1 ? 's' : ''}`;
  const userList = document.querySelectorAll('#user-list li');
  userList.forEach((el) => {
    if (el.textContent === username) {
      el.remove();
    }
  });
});
