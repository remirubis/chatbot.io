export default class User {
  constructor(name, avatar = '', createdAt = Date.now(), updatedAt = Date.now()) {
    this.name = name;
    this.avatar = avatar;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}
