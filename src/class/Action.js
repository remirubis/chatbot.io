import Message from './Message';

export default class Action {
  constructor(cmd, param, chat, bot, actions, executorSocket) {
    const {
      title,
      desc,
      cmds,
      action
    } = actions;

    this.title = title;
    this.desc = desc;
    this.cmds = cmds;
    this.action = action;
    this.executorSocket = executorSocket;

    this.messages = document.querySelector('#messages');

    this.runAction(cmd, param, chat, bot);
  }

  /*
   * Execute bot action configured in data const
   *
   * @param {String} cmd Command who executed
   * @param {String} param Parameter who used by user
   * @param {Chat} chat Chat instance for send messages in front
   * @param {Bot} bot Bot instance for get bot data
   *
   */
  async runAction(cmd, param, chat, bot) {
    if (this.cmds.includes(cmd)) {
      const returnData = this.action(param);
      const msg = new Message(returnData, bot);

      if (returnData instanceof Promise) {
        returnData.then((value) => {
          msg.msg = value;
          this.messages.innerHTML += chat.receiveMessage(msg);
          this.executorSocket.emit('message', msg);
          document.querySelector('#messages li:last-child').scrollIntoView();
        }, (error) => {
          msg.msg = error;
          this.messages.innerHTML += chat.receiveMessage(msg);
          document.querySelector('#messages li:last-child').scrollIntoView();
        });
      } else {
        setTimeout(() => {
          this.messages.innerHTML += chat.receiveMessage(msg);
          this.executorSocket.emit('message', msg);
          document.querySelector('#messages li:last-child').scrollIntoView();
        }, 500);
      }
    }
  }
}
